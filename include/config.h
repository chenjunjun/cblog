#ifndef CONFIG_H_
#define CONFIG_H_

//调整线程数量
#define THREAD_NUM 10

//调整队列容量
#define QUEUE_LEN 100

//定义日志级别
//#define LOG_LEVEL 1
#define LOG_LEVEL 2
//#define LOG_LEVEL 3

//数据文件
#define DATA_FILE "blog.db"

//定义端口
#define HTTP_PORT 8089

//定义发表博文的地址
#define POST_URI "post"

#endif
