
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <linux/in.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "cblog.h"

#ifdef T
#undef T
#endif
#define T int
#include "util/queue.i"
#undef T

static int set_nonblock(int fd){
	int fl = fcntl(fd, F_GETFL, 0);
	if(-1 == fl){
		LOG_ERR("fcntl F_GETFL error:%m\n");
		return fl;
	}
	fl |= O_NONBLOCK;
	if(-1 == fcntl(fd, F_SETFL, fl)){
		LOG_ERR("fcntl F_SETFL error:%m\n");
		return fl;
	}

	return 0;
}

void *thread(void* arg){
	queue* que = (queue*)arg;
	while(1){
		int cfd = queue_remove(que);
		set_nonblock(cfd);
		http_put_response(http_handle(http_get_request(cfd)), cfd);
		sleep(1);
		close(cfd);	
	}
}

static int comm_start(){
	int ret = 0;
	int cfd = 0;
	int sfd = 0;
	struct sockaddr_in s_add, c_add;
	int sin_size;
	unsigned short port = HTTP_PORT;

	LOG_DEBUG("begin!\n");

	//1.建立socket 
	sfd = socket(AF_INET, SOCK_STREAM, 0);
	if(-1 == sfd){
		LOG_ERR("socket build error:%m\n");
		return -1;
	}
	LOG_DEBUG("socket build ok!\n");
	
	//2. 创建服务器地址信息
	memset(&s_add, 0, sizeof(struct sockaddr_in));
	s_add.sin_family = AF_INET;
	s_add.sin_addr.s_addr = INADDR_ANY;/* 这里使用本机所有地址*/
	s_add.sin_port = htons(port);
	LOG_DEBUG("server address build ok!\n");
	
	//3. 绑定服务器地址
	ret = bind(sfd, (struct sockaddr*)(&s_add), sizeof(struct sockaddr));
	if(-1 == ret){
		LOG_ERR("bind error:%m\n");
		return -1;
	}
	LOG_DEBUG("bind ok!\n");
	
	//4. 开始监听
	ret = listen(sfd, 5);
	if(-1 == ret){
		LOG_ERR("listen error:%m\n");
		return -1;
	}
	LOG_DEBUG("listen ok!\n");
	LOG_INFO("server start ... \n");

	//建立线程池
	queue* que = queue_init(QUEUE_LEN);
	int i = 0;
	for(i = 0; i < THREAD_NUM; i++){
		pthread_t tid;
		pthread_create(&tid, NULL, thread, que);
	}
	
	//5. 开始接受客户端请求
	while(1){
		cfd = accept(sfd, NULL, NULL);
		if(-1 == cfd){
			LOG_ERR("accept error:%m\n");
			return -1;
		}
		LOG_DEBUG("accept ok!\n");
		queue_insert(que, cfd);

	}
	
	return 0;
}

int main(){
	comm_start();
}
