### 获取

### 需求

* linux
* gcc & gcc-c++
* cmake 2.6 
* make

### 安装

	$ tar zxvf cblog.tar.gz
	$ mkdir build
	$ cd build
	$ cmake -D CMAKE_INSTALL_PREFIX=you_install_dir ..
	$ make install

### 运行

	$ cd you_install_dir
	$ sh run.sh

### 使用

* 默认端口为 8089 端口
* 发布博客的网址默认为 http://host:port/post
* 可以配置 include/config.h 修改
