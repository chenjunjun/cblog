
#include "blog.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include "cblog.h"

static char* get_html(const char* file_name){
	//const char* model = "HTTP/1.0 200 OK\r\nContent_type:text/html\r\nContent-Length:%d\r\n\n%s";

	//读取文件内容
	int fd = open(file_name, O_RDONLY, 0);
	if(-1 == fd){
		LOG_ERR( "open file:%s error:%m\n", file_name);
		return NULL;
	}
	struct stat file_stat;
	int ret = fstat(fd, &file_stat);
	if(-1 == ret){
		LOG_ERR( "get file stat error:%m\n");
		return NULL;
	}

	int file_size = file_stat.st_size + 1;
	LOG_DEBUG( "file size:%d\n", file_size);
	char* file_contect = (char*)malloc(file_size);
	memset(file_contect, 0, file_size);
	ret = read(fd, file_contect, file_size - 1);
	if(-1 == ret){
		LOG_ERR( "read file error:%m\n");
		free(file_contect);
		return NULL;
	}
	LOG_DEBUG( "file_contect:%s\n", file_contect);

	//组装响应报文
	/*
	const char* model = "HTTP/1.0 200 OK\r\nContent_type:text/html\r\n\r\n%s";
	int req_size = strlen(model) + file_size;
	LOG_DEBUG( "rep size:%d\n", req_size);
	char* rep = (char*)malloc(req_size);
	LOG_DEBUG( "ni mei\n");
	snprintf(rep, req_size, model, file_contect);
	LOG_DEBUG( "html_contect:%s\n", rep);
	free(file_contect);
	*/

	return file_contect;
}

static const char* blog_model = "<h1>%s</h1><p class=\"post\"><pre>%s</pre><p>";

static char* blog_default(char* parameter){
	post_list* p_ls = post_get_all();
	int len = post_list_size(p_ls);
	LOG_DEBUG("post 数量:%d\n", len);
	LOG_DEBUG("post 1\n");
	int post_html_len = 0;
	int post_num = len > 10 ? 10: len;
	int i = 0;
	for(i = 0; i < post_num; i++){
		post* p = post_list_get(p_ls, i);
		post_html_len += strlen(p->title);
		post_html_len += strlen(p->content);
		post_html_len += strlen(blog_model);
	}
	char* post_html = (char*)malloc(post_html_len);
	memset(post_html, 0, post_html_len);
	for(i = 0; i < post_num; i++){
		post* p = post_list_get(p_ls, i);
		sprintf(post_html + strlen (post_html), blog_model, p->title, p->content);
		LOG_DEBUG("post_html:%s\n", post_html);
	}

	char* html_model = get_html("../template/index.html");
	int html_len = strlen(html_model) + post_html_len;
	char* html = (char*)malloc(html_len);
	memset(html, 0, html_len);

	LOG_DEBUG("开始渲染\n");
	sprintf(html, html_model, post_html);
	LOG_DEBUG("html:%s\n", html);

	return html;
}

static char ctoi(char c){
	if(c >= 'A'){
		c = 10 + (c - 'A');
	}else{
		c = c - '0';
	}

	return c;
}

static unsigned char* url_decoder(unsigned char* data){
	int len = strlen(data) + 1;
	unsigned char* result = (unsigned char*)malloc(len);
	memset(result, 0, len);
	int i = 0;
	int j = 0;
	LOG_DEBUG("len:%d\n", len);
	for(i = 0, j = 0; i < len - 1 ; i++, j++){
		unsigned char c = data[i];
		if(data[i] == '%'){
			c = ctoi(data[++i]);
			c = c << 4;
			c += ctoi(data[++i]);
		}else if(data[i] == '+'){
			c = ' ';
		}
		result[j] = c;
	}

	LOG_DEBUG("result:%s\n", result);

	return result;
}


static char* blog_post(char* parameter){
	if(NULL == parameter){
		char* file_content = get_html("../template/post.html");
		int html_size = strlen(file_content) + 20;
		char* html = (char*)malloc(html_size);
		memset(html, 0, html_size);
		sprintf(html, file_content, POST_URI);

		return html;
	}

	/*
	int para_num = 0;
	char* tmp = parameter;
	while(NULL != (tmp = strchr(tmp, '&'))){
		para_num++;
	}

	LOG_DEBUG("parameter number:%d\n", para_num);
	if(para_num != 2){
		return NULL;
	}
	*/

	char* title = parameter;
	char* content = NULL;
	if(0 == strncmp(title, "title=", 6)){
		content = strchr(title, '&');
		*content = 0;
		content += 1;
		title += 6;
	}else{
		return NULL;
	}
	LOG_DEBUG("title:%s\n", title);

	char* category = NULL;
	if(0 == strncmp(content, "content=", 8)){
		category = strchr(content, '&');
		*category = 0;
		category += 1;
		content += 8;
	}else{
		return NULL;
	}

	if(0 == strncmp(category, "category=", 9)){
		category += 9;
	}else{
		return NULL;
	}

	LOG_DEBUG("content:%s\n", content);
	LOG_DEBUG("category:%s\n", category);

	title = url_decoder(title);
	content = url_decoder(content);
	category = url_decoder(category);

	post* p = post_create(title, category, content);
	post_show(p);
	//post_insert(ls, p);

	return NULL;
}

static char* blog_blog(char* parameter){
	/*
	char* blog_html = get_html("../template/blog.html");
	LOG_DEBUG("获取模版:%s\n", blog_html);
	if(0 == strncmp(parameter, "id=", 3)){
		char* end = strchr(parameter, '&');
		int len = 0;
		if(NULL == end){
			len = strlen(parameter);
		}else{
			len = end - parameter - 3;
		}
		char* v = (char*)malloc(len+1);
		memset(v, 0, len+1);
		memcpy(v, parameter + 3, len);
		int id = atoi(v);
		free(v);

		post* p = post_get_by_id(id);

		int html_len = 10240;
		char* html = (char*)malloc(html_len);
		int post_html_len = 1024;
		char* post_html = (char*)malloc(post_html_len);
		memset(html, 0, html_len);
		memset(post_html, 0, post_html_len);
		sprintf(post_html, blog_model, p->title, p->content);
		sprintf(html, blog_html, post_html);
		free(post_html);

		return html;
	}*/

	return NULL;
}

static char* blog_category(char* parameter){
}

char* blog_handle(const char* program, char* parameter){
	LOG_DEBUG("进入到博客处理阶段:开始分发任务!\n");

	static char* (*handle)(char*);
	if( NULL == program ){
		LOG_DEBUG("请求为空,分配给默认处理器!\n");
		handle = blog_default;
	}else if(0 == strcmp(program, POST_URI)){
		LOG_DEBUG("分配文章发布!\n");
		handle = blog_post;
	}else if(0 == strcmp(program, "blog")){
		LOG_DEBUG("分配给博可!\n");
		handle = blog_blog; 
	}else if(0 == strcmp(program, "category")){
		LOG_DEBUG("分配给分类!\n");
		handle = blog_category;
	}else{
		LOG_DEBUG("没有配置的处理器:分配给默认处理器!\n");
		handle = blog_default;
	}

	LOG_DEBUG("进入到博客处理阶段:开始处理!\n");
	char* html =  handle(parameter);
	if(NULL == html){
		return blog_default(NULL);
	}
}

