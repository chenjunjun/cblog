
#include <stdlib.h>

#include <semaphore.h>


typedef struct{
	T* buf;
	int n;
	int front;
	int rear;
	sem_t mutex;
	sem_t slots;
	sem_t items;
}queue;

queue* queue_init(int size){
	queue* que = (queue*)malloc(sizeof(queue));
	que->n = size;
	que->buf = (T*)malloc(que->n * sizeof(T));
	que->front = 0;
	que->rear = 0;
	sem_init(&que->mutex, 0, 1);
	sem_init(&que->slots, 0, que->n);
	sem_init(&que->items, 0, 0);

	return que;
}

void queue_insert(queue* que, T item){
	sem_wait(&que->slots);
	sem_wait(&que->mutex);
	que->buf[(++que->rear)%(que->n)] = item;
	sem_post(&que->mutex);
	sem_post(&que->items);
}

T queue_remove(queue* que){
	sem_wait(&que->items);//检查是否存在数据
	sem_wait(&que->mutex);//锁定buf
	T t = que->buf[(++que->front)%(que->n)];
	sem_post(&que->mutex);//对buf解锁
	sem_post(&que->slots);//增加空项

	return t;
}
