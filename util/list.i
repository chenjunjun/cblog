
#include <stdlib.h>

#include "log.h"

typedef struct node{
	struct T* data;
	struct node* next;	
}node;


static node* node_create(struct T* data){
	node* n = (node*)malloc(sizeof(node));
	n->data = data;
	n->next = NULL;
	
	return n;
}

//LIST* list_create(){
LIST* LIST_CREATE(){
	node* n = (node*)malloc(sizeof(node));
	n->next = NULL;
	
	return n;
}

//void list_add(LIST* ls, struct T* data){
void LIST_ADD(LIST* ls, struct T* data){
	if(NULL == ls){
		LOG_INFO("ls == NULL!\n");
		return;
	}

	node* new_node = node_create(data);
	new_node->next = ls->next;
	ls->next = new_node;
}



//struct T* list_find(LIST* ls, struct T* target, int (*comp)(struct T* e, struct T* t)){
struct T* LIST_FIND(LIST* ls, void* target, int (*comp)(struct T* e, void* t)){
	if(NULL == ls){
		LOG_INFO("ls == NULL!\n");
		return;
	}

	int ret = 0;
	node* n = ls->next;
	while(n){
		ret = comp(n->data, target);
		if(0 == ret){
			return n->data;
		}
		n = n->next;
	}
}


//int list_size(LIST* ls){
int LIST_SIZE(LIST* ls){
	if(NULL == ls){
		LOG_INFO("ls == NULL!\n");
		return;
	}

	int size = 0;
	node* n = ls->next;
	while(n){
		n = n->next;
		size++;
	}
	
	return size;
}

struct T* LIST_GET(LIST* ls, int index){
	if(NULL == ls){
		LOG_INFO("ls == NULL!\n");
		return;
	}

        if(index >= LIST_SIZE(ls)){
                return NULL;
        }
        int i = 0;
        node* n = ls->next;
        for(i = 0; i < index; i++){
                n = n->next;
        }

        return n->data;
}
