#ifndef UTIL_LOG_H_
#define UTIL_LOG_H_

#include <stdio.h>

#ifndef LOG_LEVEL
#define LOG_LEVEL 1
#endif

#define INFO(out, str, args...) \
	fprintf(out, "file:%s, line:%d:\t" str, \
			__FILE__, __LINE__, ##args)

#if 1 <= LOG_LEVEL
#define LOG_ERR(str, args...) INFO(stderr, str, ##args)
#else
#define LOG_ERR(str, args...)
#endif

#if 2 <= LOG_LEVEL
#define LOG_INFO(str, args...) INFO(stdout, str, ##args)
#else
#define LOG_INFO(str, args...)
#endif

#if 3 <= LOG_LEVEL
#define LOG_DEBUG(str, args...) LOG_INFO(str, ##args)
#else
#define LOG_DEBUG(str, args...)
#endif

/*
int main(){
	LOG_ERR("ni:%d, mei!\n", 1);
	LOG_INFO("ni:%d, mei!\n", 2);
	LOG_DEBUG("ni:%d, mei!\n", 3);
}
*/

#endif
