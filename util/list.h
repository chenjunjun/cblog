
#define TYPE_LIST_CREATE(type) type ## _list_create
#define T_LIST_CREATE(type) TYPE_LIST_CREATE(type)
#define LIST_CREATE T_LIST_CREATE(T)

#define TYPE_LIST_ADD(type) type ## _list_add
#define T_LIST_ADD(type) TYPE_LIST_ADD(type)
#define LIST_ADD T_LIST_ADD(T)

#define TYPE_LIST_FIND(type) type ## _list_find
#define T_LIST_FIND(type) TYPE_LIST_FIND(type)
#define LIST_FIND T_LIST_FIND(T)

#define TYPE_LIST_SIZE(type) type ## _list_size
#define T_LIST_SIZE(type) TYPE_LIST_SIZE(type)
#define LIST_SIZE T_LIST_SIZE(T)

#define TYPE_LIST_GET(type) type ## _list_get
#define T_LIST_GET(type) TYPE_LIST_GET(type)
#define LIST_GET T_LIST_GET(T)

#define TYPE_LIST(type) type ## _list
#define T_LIST(type) TYPE_LIST(type)
#define LIST T_LIST(T)

struct node;

typedef struct node LIST;

LIST* LIST_CREATE();
void LIST_ADD(LIST* ls, struct T* data);
struct T* LIST_FIND(LIST* ls, void* target, int (*comp)(struct T* e, void* t));
int LIST_SIZE(LIST* ls);
struct T* LIST_GET(LIST* ls, int index);

