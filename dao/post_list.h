#ifndef DAO_POST_LIST_H_
#define DAO_POST_LIST_H_

struct post;

#ifdef T
#undef T
#endif
#define T post
#include "../util/list.h"

/*
post_list* post_list_create();

void post_list_add(post_list* ls, T* data);

T* post_list_find(post_list* ls, T* target, int (*comp)(T* e, T* t));

int post_list_size(post_list* ls);

T* post_list_get(post_list* ls, int index);
*/

#endif
