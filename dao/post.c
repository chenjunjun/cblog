
#include "post.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cblog.h"

static int next_post_id();
static void post_show_list();
static void post_insert(post* p);
static post* post_malloc(int id,long date, char* title, char* category, char* content );

static post* post_malloc( int id, long date, char* title, char* category, char* content ){
	post* p = (post*)malloc(sizeof(post));
	p->id = id;
	p->date = date;
	p->title = title;
	p->category = category;
	p->content = content;

	return p;
}

post* post_create(char* title, char* category, char* content ){
	int id = next_post_id();
	if(-1 == id){
		LOG_ERR( "分配 post Id 失败!!!\n");
		return NULL;
	}

	post* p = post_malloc(id, time(NULL), title, category, content);

	post_insert(p);
	return p;
}

void post_free(post* p){
	if(NULL == p){
		return;
	}
	if(NULL != p->category){
		free(p->category);
	}

	if(NULL != p->title){
		free(p->title);
	}

	if(NULL != p->content){
		free(p->content);
	}

	free(p);
}

static int next_post_id(){
	static int post_id = -1;
	if(-1 == post_id){
		post_list* ls = post_get_all();
		int size = post_list_size(ls);
		while(size){
			size--;
			post* p = post_list_get(ls, size);
			if(post_id < p->id){
				post_id = p->id;
			}
		}
	}
	post_id++;
	return post_id;
}



void post_show(post* p){
	if(NULL == p){
		LOG_ERR( "p == NULL\n");
	}
	LOG_DEBUG("id:%d\n", p->id);
	LOG_DEBUG("title:%s\n", p->title);
	LOG_DEBUG("date:%ld\n", p->date);
	LOG_DEBUG("category:%s\n", p->category);
	LOG_DEBUG("content:%s\n", p->content);
}

static int next_int32(FILE* file){
	int tmp = 0;
	fread(&(tmp), 1, sizeof(tmp), file);

	return tmp;
}
static int64_t next_int64(FILE* file){
	int64_t tmp = 0;
	fread(&(tmp), 1, sizeof(tmp), file);

	return tmp;
}
static char* next_string(FILE* file){
	short len = 0;
	fread(&(len), 1, sizeof(len), file);

	char* str = (char*)malloc(len + 1);
	memset(str, 0, len+1);

	fread(str, 1, len, file);

	return str;
}

static post* next_post(FILE* file){
	if(NULL == file){
		LOG_ERR("file == NULL!\n");
		return NULL;
	}
	post* e = (post*)malloc(sizeof(post));
	memset(e, 0, sizeof(post));

	//读取id
	e->id = next_int32(file);
	if(feof(file)){
		LOG_INFO("read file eof:%m\n");
		free(e);
		e = NULL;
		return NULL;
	}

	//读取日期
	e->date = next_int64(file);
	if(ferror(file)){
		LOG_ERR("read file error:%m\n");
		free(e);
		e = NULL;
		return NULL;
	}

	//读取标题
	e->title = next_string(file);
	if(ferror(file)){
		LOG_ERR("read file error:%m\n");
		free(e);
		e = NULL;
		return NULL;
	}
	//读取分类
	e->category = next_string(file);
	if(ferror(file)){
		LOG_ERR("read file error:%m\n");
		free(e);
		e = NULL;
		return NULL;
	}
	//读取内容
	e->content = next_string(file);
	if(ferror(file)){
		LOG_ERR("read file error:%m\n");
		free(e);
		e = NULL;
		return NULL;
	}
	
	return e;
}

static post_list* load_data(){
	FILE* file= fopen(DATA_FILE, "r");
	if(NULL == file){
		LOG_ERR("open file error:%m file:%s", DATA_FILE);
		return NULL;
	}

	int ret = fseek(file, 0, SEEK_SET);
	if(0 != ret){
		LOG_ERR("fseek file error!\n");
		return NULL;
	}

	post_list* ls = post_list_create();
	post* e = next_post(file);
	while(e){
		post_list_add(ls, e);
		e = next_post(file);
	}

	fclose(file);

	return ls;
}

static void store_int32(FILE* file, int32_t data){
	fwrite(&(data), 1, sizeof(data), file);
}
static void store_int64(FILE* file, int64_t data){
	fwrite(&(data), 1, sizeof(data), file);
}
static void store_string(FILE* file, char* str){
	short len = strlen(str);
	fwrite(&(len), 1, sizeof(len), file);

	fwrite(str, 1, len, file);
}

static void store_post(post* e, FILE* file){
	store_int32(file, e->id);
	if(ferror(file)){
		LOG_ERR("write file error:%m\n");
		return;
	}
	store_int64(file, e->date);
	if(ferror(file)){
		LOG_ERR("write file error:%m\n");
		return;
	}
	store_string(file, e->title);
	if(ferror(file)){
		LOG_ERR("write file error:%m\n");
		return;
	}
	store_string(file, e->category);
	if(ferror(file)){
		LOG_ERR("write file error:%m\n");
		return;
	}
	store_string(file, e->content);
	if(ferror(file)){
		LOG_ERR("write file error:%m\n");
		return;
	}
}

static void store_data(post_list* ls){
	FILE* file= fopen(DATA_FILE, "w");
	if(NULL == file){
		LOG_ERR("open file error:%m file:%s", DATA_FILE);
		return ;
	}
	int ret = fseek(file, 0, SEEK_SET);
	if(0 != ret){
		LOG_ERR("fseek file error!\n");
		return ;
	}

	int size = post_list_size(ls);
	int i = 0;
	for(i = 0; i < size; i++){
		store_post(post_list_get(ls, i), file);
	}

	fflush(file);
	fclose(file);

	return;	
}

//获取所有文章
post_list* post_get_all(){
	static post_list* ls = NULL;
	if(NULL == ls){
		ls = load_data();
		if(NULL == ls){
			ls = post_list_create();
		}
	}

	return ls;
}

//插入新的文章
static void post_insert(post* p){
	post_list* ls = post_get_all();
	post_list_add(ls, p);
	store_data(ls);
	post_show_list();
}

static void post_show_list(){
	post_list* ls = post_get_all();
	if(NULL == ls){
		LOG_DEBUG("ls == NULL\n");
		return;
	}
	LOG_DEBUG("****** show log begin *******\n");
	int size = post_list_size(ls);
	int i = 0;
	for(i = 0; i < size; i++){
		post_show(post_list_get(ls, i));
		LOG_DEBUG("*************\n");
	}
	LOG_DEBUG("****** show log end *******\n");
}
/*
int main(){
	post_list* ls = post_get_all();
	post_show_list(ls);

	post* e = (post*)malloc(sizeof(post));
	e->id = 1;
	e->date = 1;
	e->title = "title";
	e->category = "cate";
	e->content = "con";

	post_insert(ls, e);
	post_show_list(ls);

	store_data(ls);

	return 0;
}
*/
