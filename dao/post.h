
#ifndef POST_H_
#define POST_H_

#include "post_list.h"

typedef struct post{
	int id;
	char* title;
	long date;
	char* category;
	char* content;
}post;

//释放一个post对象
void post_free(post*);

//创建一个post
post* post_create(char* title, char* category, char* content);

//打印一个post
void post_show(post*);

//获取所有文章
post_list* post_get_all();

/*
//根据ID获取文章
post* post_get_by_id(int id);

//根据分类获取文章列表
post* post_get_by_category(const char* category);

//获取所有文章
post** post_get_all(int* len);

//插入新的文章
void post_insert(post* p);

//更新文章
void post_update(post* p);

//删除文章
void post_delete(int id);
*/
#endif
