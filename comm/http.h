
#ifndef HTTP_H_
#define HTTP_H_

struct request;

struct response;


struct request* http_get_request(int socket);

struct response* http_handle(struct request* req);

void http_put_response(struct response* resp, int socket);

#endif
