
#include "http.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>

#include <unistd.h>
#include <fcntl.h>

#include "cblog.h"



typedef struct request{
	char* program;
	char* parameter;
}request;

typedef struct response{
	char* html;
}response;


static request* request_malloc(){
	request* req = (request*)malloc(sizeof(request));
	req->parameter = NULL;
	req->program = NULL;

	return req;
}

static void request_free(request** req){
	if(NULL == req || NULL == *req){
		return;
	}
	request* r = *req;
	if(NULL != r->parameter){
		free(r->parameter);
	}

	if(NULL != r->program){
		free(r->program);
	}

	free(r);	
	*req = NULL;
}

static response* response_malloc(){
	response* resp = (response*)malloc(sizeof(response));
	resp->html = NULL;

	return resp;	
} 

static void response_free(response** resp){
	if(NULL == resp|| NULL == *resp){
		return ;
	}
	response* r = *resp;
	if(NULL != r->html){
		free(r->html);
	}
	
	free(r);
	*resp = NULL;
}


static void send_data(int socket, const char* buf, int len){
	int send_len = 0;
	time_t current_time = time(NULL);
	while(len > send_len){
		if(time(NULL) - current_time > 10){
			return; 
		}
		int ret = write(socket, buf + send_len, len - send_len);
		if(-1 == ret ){
			if(errno == EAGAIN){
				continue;
			}

			LOG_ERR("send error:%m\n");
			return;
		}
		send_len += ret;
	}
}
/*
 * 返回的数据需要调用者释放
 * */
static char* recv_data(int socket, int len){
	char* buf = (char*)malloc(len + 1);
	memset(buf, 0, len + 1);
	int read_len = 0;
	while(len != read_len){
		int ret = read(socket, buf + read_len, len - read_len);
		if(-1 == ret){
			if(errno == EAGAIN){
				continue;
			}
			LOG_ERR("socket error:%m\n");
			free(buf);
			return NULL;
		}
		read_len += ret;
	}
	
	return buf;
}
/*
 * 返回的数据需要释放
 * */
static char* get_line(int socket){
	int buf_len = 500;
	char* buf = (char*)malloc(buf_len);
	memset(buf, 0, buf_len);
	int i = 0;
	for(i = 0; i < buf_len; i++){
		char* c = recv_data(socket, 1);
		if(NULL == c){
			LOG_ERR("read socket error:%m\n");
			free(buf);
			return NULL;
		}
		buf[i] = *c;
		if('\r' == buf[i] ){
			i++;
			c = recv_data(socket,  1);
			if(NULL == c){
				LOG_ERR("read socket error:%m\n");
				free(buf);
				return NULL;
			}
			buf[i] = *c;
			if('\n' == buf[i]){
				return buf;
			}
		}
	}

	free(buf);

	return NULL;
}

/*
 * 获取 POST 请求
 * 返回的数据需要释放
 * */
static request* get_post_req(char* http_head, int socket){

	//解析获取处理程序
	char* begin = strchr(http_head, '/') + 1;
	char* end = strchr(begin, ' ');
	if(begin == end){
		return NULL;
	}
	request* req = request_malloc();

	int param_len = end - begin + 1;
	req->program = (char*)malloc(param_len);
	memset(req->program, 0, param_len);
	memcpy(req->program, begin, end - begin);

	int parameter_len = 0;
	while(1){
		char* line = get_line(socket);
		if(NULL == line){
			LOG_ERR("bad http request!\n");
		}
		const char* len_line_key = "Content-Length:";
		if(0 == strncmp(line, len_line_key, strlen(len_line_key)) ){
			LOG_DEBUG( "获取到 post 的长度:%s\n", line);
			char* len_value = line + strlen(len_line_key) + 1;
			parameter_len = atoi(len_value);
			LOG_DEBUG( "长度:%d\n", parameter_len);
			break;
		}
	}


	//解析获取参数
	int stat = 0;
	while(1){
		char c = '\0';
		char* pc = recv_data(socket, 1);
		if(NULL == pc) {
			LOG_ERR("socket error:%m\n");
			return NULL;
		}
		c = *pc;
		if(0 == stat && '\r' == c){
			stat = 1;
		}else if(1 == stat && '\n' == c){
			stat = 2;
		}else if(2 == stat && '\r' == c){
			stat = 3;
		}else if(3 == stat && '\n' == c){
			break;
		}else {
			stat = 0;
		}

	}

	req->parameter = recv_data(socket, parameter_len);

	return req;
}
/*
 * 获取 GET 请求
 * 返回的数据需要释放
 * */
static request* get_get_req(char* http_header){
	LOG_DEBUG( "1\n");
	char* begin = strchr(http_header, '/') + 1;
	LOG_DEBUG( "2\n");
	char* end = strchr(begin, ' ');

	LOG_DEBUG( "3\n");
	if(begin == end){
		LOG_DEBUG( "位置错误, begin - end :%d!\n", begin - end);
		return NULL;
	}

	int url_len = end - begin + 1;
	char* url = (char*)malloc(url_len);
	memset(url, 0, url_len);
	memcpy(url, begin, url_len - 1);

	LOG_DEBUG( "开始解析处理程序, get:%s!\n", url);
	//解析处理程序
	request* req = request_malloc();
	char* que = strchr(url, '?');
	if(NULL == que){
		req->program = url;
		req->parameter = NULL;
		return req;
	}

	int propram_len = que - url + 1;
	req->program = (char*)malloc(propram_len);
	memset(req->program, 0, propram_len);
	memcpy(req->program, url, propram_len - 1);

	//解析参数
	LOG_DEBUG( "开始解析参数!\n");
	int parameter_len = end - begin - (propram_len - 1) - 1 + 1;
	req->parameter = (char*)malloc(parameter_len);
	memset(req->parameter, 0, parameter_len);
	memcpy(req->parameter, que + 1, parameter_len - 1);

	free(url);
	return req;
}


request* http_get_request(int socket){
	char* line = get_line(socket);
	if(NULL == line){
		LOG_ERR("read socket error:%m\n");
		return NULL;
	}
	request* req = NULL;
	if(0 == strncmp(line, "GET", 3) ){
		LOG_DEBUG( "进入 get 解析!\n");
		req = get_get_req(line);
	}else if(0 == strncmp(line, "POST", 4)){
		LOG_DEBUG( "进入 post 解析!\n");
		req = get_post_req(line, socket);
	}else{
		LOG_DEBUG( "没有进入 get/post 解析!\n");
	}
	free(line);
	line = NULL;

	if(NULL == req){
		LOG_DEBUG("req is NULL\n");
	}else{
		LOG_DEBUG("program:%s\n", req->program);
		LOG_DEBUG("parameter:%s\n", req->parameter);
	}

	return req;
}

response* http_handle(request* req){
	if(NULL == req){
		req = request_malloc();
	}
	char* html = blog_handle(req->program, req->parameter);
	request_free(&req);
	response* resp = response_malloc();
	resp->html = html;

	return resp;
}

void http_put_response(response* resp, int socket){
	const char* model = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection: close\r\nContent-Length: %d\r\n\r\n";
	int http_head_len = strlen(model) + 10;
	char* http_head = (char*)malloc(http_head_len);
	memset(http_head, 0, http_head_len);
	int html_len = strlen(resp->html);
	sprintf(http_head, model, html_len);

	send_data(socket, http_head, strlen(http_head));
	LOG_DEBUG("http head:\n%s\n", http_head);
	free(http_head);

	send_data(socket, resp->html, html_len);
	response_free(&resp);
}


